import logo from "./logo.svg";
import "./App.css";
import { Input } from "./components/input";
import { Display } from "./components/display";
import { useState } from "react";

function App() {
  const [list, setList] = useState([]);
  const [input, setInput] = useState(false);

  return (
    <div className="App">
      <header className="App-header">
        <Input setInput={setInput} />
        <Display input={input} list={list} setList={setList} />
      </header>
    </div>
  );
}

export default App;
