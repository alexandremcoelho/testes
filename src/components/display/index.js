export const Display = ({ input, list, setList }) => {
  //   console.log(input);
  //   console.log(list);

  return (
    <>
      <button disabled={!input} onClick={() => setList([...list, input])}>
        botao
      </button>
      <ul>
        {list.map((item, index) => (
          <li key={index}>{item}</li>
        ))}
      </ul>
    </>
  );
};
