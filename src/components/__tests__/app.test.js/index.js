import { render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import App from "../../../App";

describe("testando app", () => {
  test("botao nao pode ser clicado quando o input esta vazio", () => {
    render(<App />);

    expect(screen.getByRole("button")).toBeDisabled();
  });
  test("input deve adicionar itens na lista", async () => {
    render(<App />);

    userEvent.type(screen.getByRole("textbox"), "texto");
    userEvent.click(screen.getByRole("button"));

    const myList = await screen.findAllByRole("listitem");

    expect(myList).toHaveLength(1);
  });
});
