export const Input = ({ setInput }) => {
  return (
    <input
      onChange={(e) => {
        setInput(e.target.value);
      }}
    ></input>
  );
};
